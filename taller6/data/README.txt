Si llega nueva información para una llave que ya existe en la tabla de hash
el método encargado de esta función no hace acción alguna.

El criterio de crecimiento de la tabla es que si se lega a alcanzar el factor
de carga máximo, inmediatamente se duplica su capacidad y se procede a insertar
nuevas llaves. Escogí la función de encadenamiento (separate chaining) ya que en este escenario es mucho más recomendado: escenario en el cual el número de registros es cercado a la capacidad de la tabla.

SI tiene sentido hacer una tabla de listas, en una tabla Hash, al tener una llave asociada a un Objeto (tipo Object) podemos hacer que una llave esté relacionada a una lista que contiene objetos, y en ciertos casos puede ser útil, como el de este taller por ejemplo, en el que bajo una llave (el número de teléfono, por ejemplo) podemos encontrar diferentes datos (nombre, apellido) en una tabla.

Las tablas de Hash no iteran muy buen, por lo tanto hacer Sorting en ellas es un dolor de cabeza, no son recomendables cuando queremos alojar datos individuales sin ningún dato adicional relacionado a cada dato en sí, por ejemplo, si quiero almacenar una lista de códigos solo por almacenarla, no es recomendable utilizar Hash ya que no se utilizarían sus beneficios (llave, valor).