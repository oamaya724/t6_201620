package taller.estructuras;

import java.util.ArrayList;

  

  


public class TablaHash2Values<K extends Comparable<K> , Wrapper> {

	

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	
	private ArrayList<NodoHash<K, Wrapper>> bucketArray;
	

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash2Values(){
		count = 0;
		capacidad = 3000000;
		
	}

	@SuppressWarnings("unchecked")
	public TablaHash2Values(int capacidad, float factorCargaMax) {
		bucketArray = new ArrayList<>();
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		
		for (int i = 0; i < capacidad; i++)
			bucketArray.add(null);
		

	}

	public void put(K llave, Wrapper wp){
		int bucketIndex = hash(llave);
		NodoHash<K, Wrapper> head = bucketArray.get(bucketIndex);
		
		// Se verifica si la llave ya esta presente.
		
		while (head != null)
		{
			if (head.getLlave().equals(llave))
			{
				Wrapper s = head.getValor();
				s = wp;
				return;
			}
			head = head.next;
		}
		
		// Se inserta la llave
		count++;
		head = bucketArray.get(bucketIndex);
		NodoHash<K,Wrapper> nuevoNodo = new NodoHash<K,Wrapper>(llave, wp);
		nuevoNodo.next = head;
		bucketArray.set(bucketIndex, nuevoNodo);
		
		// Si el factor de carga sobre pasa el factor de carga máximo
		// se duplica el tamanio de la tabla hash
		if (factorCarga > factorCargaMax)
		{
			ArrayList<NodoHash<K,Wrapper>> temp = bucketArray;
			bucketArray = new ArrayList<>();
			capacidad = 2 * capacidad;
			count = 0;
			for (int i = 0; i < capacidad; i++)
				bucketArray.add(null);
			for (NodoHash<K,Wrapper> headNode : temp)
			{
				while (headNode != null)
				{
					put(headNode.getLlave(), headNode.getValor());
					headNode = headNode.next;
				}
			}
		}
		
		
		
	}

	public Object get(K llave){
		int bucketIndex = hash(llave);
		NodoHash<K,Wrapper> head = bucketArray.get(bucketIndex);
		
		while(head != null)
		{
			if (head.getLlave().equals(llave))
				return head.getValor();
		}
		return null;
		
	

		
	}

	public Object delete(K llave){
		int bucketIndex = hash(llave);
		NodoHash<K,Wrapper> head = bucketArray.get(bucketIndex);
		NodoHash<K,Wrapper> prev = null;
		while (head != null)
		{
			if (head.getLlave().equals(llave))
				break;
			
			prev = head;
			head = head.next;
		}
		
		if (head == null)
			return null;
		count--;
		
		if (prev != null)
			prev.next = head.next;
		else
			bucketArray.set(bucketIndex, head.next);
		return head.getValor();
		

		
	}

	//Hash
	private int hash(K llave)
	{
		int hashCode = llave.hashCode();
		int index = hashCode % capacidad;
		return index;

	}
	

}