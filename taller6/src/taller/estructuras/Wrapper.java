package taller.estructuras;

class Wrapper {
	
	private Object value1;
	private Object value2;
	
	
    public Wrapper(Object value1, Object value2) {
       this.value1 = value1;
       this.value2 = value2;
    }

    public Object getValue1() { return this.value1; }
    public Object getValue2() { return this.value2; }
  

  
}