package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import taller.estructuras.TablaHash;




public class Main {
	
	private static TablaHash tabla;
	
	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static String rutaSalida = "./data/emergencias.csv";

	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();
			FileWriter fw = new FileWriter((new File(rutaSalida)));
			

			//TODO: Inicialice el directorio t
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				for(int j = 0; j < datos.length; j++)
				{
					fw.write(entrada);
				}
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				FileWriter fw = new FileWriter((new File(rutaSalida)));
				String in = br.readLine();
				switch (in) {
				case "1":
					br.readLine();
					fw.write(in);
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					break;
				case "2":
					Object nombre = br.readLine();
					String apellido = br.readLine();
					Comparable telefono = br.readLine();
					tabla.put(telefono, nombre);
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
